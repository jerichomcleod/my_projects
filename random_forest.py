# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 17:40:01 2018

@author: jmcle
"""
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

#df = pd.read_csv('D:\\Crypto_Project\\xrp_drag.csv')
df = pd.read_csv('D:\\Crypto_Project\\btc_drag.csv')

#df = df.drop(columns=['Price'])
try:
    df=df.drop(columns=['XRPonlyPolarity','XRPonlySubjectivity'])
except:
    pass
try:
        df=df.drop(columns=['BTConlyPolarity','BTConlySubjectivity'])
except:
    pass
#print(list(df))


df['is_train'] = np.random.uniform(0,1, len(df))<=.75
train, test = df[df['is_train']==True], df[df['is_train']==False]

features = df.columns[2:]
#print(list(features))

y = train['Up/Down']
z = test['Up/Down']
print("Train Set Length:",len(y))
print("Test Set Length:",len(z))
#print(list(y))
clf = RandomForestClassifier(n_jobs=1, random_state=None)

clf.fit(train[features], y)
clf.predict(test[features])

test['Preds']=clf.predict(test[features])
#clf.predict_proba(test[features])[0:10]

pd.crosstab(test['Up/Down'], test['Preds'], rownames=['Actual'],colnames=['Predicted'])
#%%
feat_importance=list(zip(train[features], clf.feature_importances_))
print(feat_importance)

#%%