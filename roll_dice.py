# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 17:18:24 2018

@author: Jericho
"""
import random

n_trials = 1000000
roll_results={}
for i in range(11):
    roll_results[i+2]=0
    
def roll_2():
    total=0
    for i in range(2): total+=random.randint(1,6)
    return(total)

for i in range(n_trials):
    total=roll_2()
    roll_results[total]+=1

for k,v in roll_results.items():
    v=v/n_trials
    print(k,v)

#%%
