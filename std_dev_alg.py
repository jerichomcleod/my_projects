# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 13:14:29 2018

@author: jcm2
"""

import random
import statistics
import math
import time
t10=time.time()
def moving_stdv(sample):
    m = 0
    s = 0
    N = len(sample)
    leng=range(1,N+1)
    for i in leng:
        x=sample[i-1]
        oldm=m
        m=m+(x-m)/i
        s=s+(x-m)*(x-oldm)
    s=s/N
    mov_stdev = math.sqrt(s)
    return(mov_stdev)

def standard_stdv(sample):
    sum_all = 0
    for i in sample:
        sum_all+=i
    xbar = sum_all/len(sample)
    sumvar=0
    for i in sample:
        sumvar+= (i-xbar)**2
    var = sumvar/len(sample)
    stddev= math.sqrt(var)
    return(stddev)

random.seed(12346)
sample = []
for i in range(0,216216):
    sample.append(random.randint(0,100))
    
t0 = time.time()
stdev = moving_stdv(sample)
t1 = time.time()
print(stdev)
total_time=t1-t0
print("Time for one-pass algorithm: ",total_time)


t2=time.time()
stdev2=standard_stdv(sample)
t3=time.time()
print(stdev2)
total_time2=t3-t2
print("Time for two-pass algorithm: ",total_time2)


t4=time.time()
stdev3=statistics.stdev(sample)
t5=time.time()
print(stdev3)
total_time3=t5-t4
print("Time for included algorithm: ",total_time3)

t11=time.time()
print()
total_time = t11-t10
print("Total time: ",total_time)
#%%
