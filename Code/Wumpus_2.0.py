"""
Author = Jericho McLeod
Year = 2018
"""
import random
"""
Layout:
[ 1][ 2][ 3][ 4]
[ 5][ 6][ 7][ 8]
[ 9][10][11][12]
[13][14][15][16]
Start = 13
locations = player, wumpus, pit, gold
"""
def rand_num():
    number = random.randint(1,13)
    if number >8:
        number +=1
    if number >12:
        number +=2
#    print("Making Numbers")
#    print(number)
    return(number)

def start():
    locations=list()
    play_loc = 13
    wum_loc = rand_num()
    pit_loc = rand_num()
    while pit_loc == wum_loc:
#        print("Finding pit")
        pit_loc = rand_num()
    gold_loc = rand_num()
    while gold_loc == wum_loc or gold_loc == pit_loc:
#        print("Finding gold")
        gold_loc = rand_num()
    temp_list = (play_loc,wum_loc,pit_loc,gold_loc)
    for i in temp_list:
        locations.append(i)
#    locations.append(play_loc,wum_loc,pit_loc,gold_loc)
    return(locations)

def warnings(location):
    for i in ("w","a","s","d"):
        test_loc,valid = boundary(location[0],i)
        if valid == True:
            if test_loc == location[1]:
                print("You hear growling!")
            elif test_loc == location[2]:
                print("You hear wind...")

def play_area(locations):
    play_loc,wum_loc,pit_loc,gold_loc = locations
    empty,player,wump,pit,gold =  "[ ]","[@]","[W]","[P]","[G]"
#    print(empty,player,wump,pit,gold)
#    print(play_loc,wum_loc,pit_loc,gold_loc)
#    print(locations)
    row1=str()
    row2=str()
    row3=str()
    row4=str()
    row_list = (row1,row2,row3,row4)
    x,y=1,5
    for row in row_list:
        for i in range(x,y):
            if i not in locations:
                row=row+empty
            elif i == play_loc:
                row=row+player
            elif i == wum_loc:
                row=row+empty
            elif i == pit_loc:
                row=row+empty
            elif i == gold_loc:
                row=row+empty
            else:
                print("I have experienced a massive coronary")
        print(row)
        x+=4
        y+=4
    warnings(locations)
    print(locations)

def boundary(play_loc,direction):
    right_edge = (4,8,12,16)
    left_edge = (1,5,9,13)
    top_edge = (1,2,3,4)
    bottom_edge = (13,14,15,16)
    if play_loc in right_edge and direction == "d":
        valid = False
    elif play_loc in left_edge and direction == "a":
        valid = False
    elif play_loc in top_edge and direction == "w":
        valid = False
    elif play_loc in bottom_edge and direction == "s":
        valid = False
    else:
        valid = True
        if direction == "d":
            play_loc +=1
        elif direction == "a":
            play_loc+=-1
        elif direction == "w":
            play_loc+=-4
        elif direction == "s":
            play_loc += 4
    return(play_loc,valid)

def player_move():
    check_values = ("w","a","s","d","t")
    valid = False
    while valid == False:
        move=input("Enter your move: ")
        if move in check_values:
            valid = True
            return(move)
            print()
        else: print("Dude... what? \n")

def game():
    locations = start()
    alive = 0
    while alive == 0:
        play_area(locations)
        if locations[0] == locations[1]:
            print("The wumpus ate you")
            alive +=1
            return(1)
        elif locations[0] == locations[2]:
            print("You fell into a pit")
            alive +=1
            return(2)
        elif locations[0] == locations[3]:
            print("You found gold!")
            alive +=1
            return(3)
        else:
            move_valid = False
            while move_valid == False:
                locations[0],move_valid = boundary(locations[0],player_move())
        
def main():
    gamer = 0
    wum_score = 0
    pit_score = 0
    gold_score = 0
    while gamer == 0:
        result = game()
        if result==1:
            wum_score+=1
        elif result==2:
            pit_score+=1
        else:
            gold_score+=1
        print()
        print("Wumpus: ",wum_score)
        print("Pit: ",pit_score)
        print("Gold: ",gold_score)
        play_again = input("Do you want to play again? (y/n): ")
        if play_again == "y":
            pass
        elif play_again == "n":
            gamer+=1
        else:
            print("You suck at following directions")
            gamer+=1
        
main()


#%%
