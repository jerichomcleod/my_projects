# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 15:34:09 2018

@author: Jericho
"""
# Import libraries and dataset
import pandas as pd
import numpy as np

titanic_df=pd.read_csv('C:\\Users\\Jericho\\Google Drive\\School\\titanic.csv')

#%%
# Explore data
col_names = list(titanic_df.columns.values)

"""  Select the variables you wish to explore here, 
     using 1 to show and 0 to suppress:"""
var_list = {    'Survived':                 0, 
                'Pclass':                   0, 
                'Name':                     0, 
                'Sex':                      0, 
                'Age':                      0, 
                'Siblings/Spouses Aboard':  0, 
                'Parents/Children Aboard':  0, 
                'Fare':                     1}

for i in col_names:
    if var_list.get(i) == 1:
        print(titanic_df[i].describe(include='all'))
        print()
        print(titanic_df[i].head())
        print()

print(col_names)
#%%
    
# Convert Gender to Boolean: M=0, F=1
titanic_df['Sex']=titanic_df['Sex'].replace('female',1)
titanic_df['Sex']=titanic_df['Sex'].replace('male',0)

#%%

#Create training set
titanic_df['is_train'] = np.random.uniform(0,1, len(titanic_df))<=.75
train, test = titanic_df[titanic_df['is_train']==True], titanic_df[titanic_df['is_train']==False]