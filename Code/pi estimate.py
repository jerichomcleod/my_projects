# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 09:41:08 2018

@author: jcm2
"""
#%%
import random
import time
t0 = time.time()

n = 100000000
counter = {'inside':0,'outside':0}

def rand_coords():
    x = random.randint(-1000000,1000000)/1000000
    y = random.randint(-1000000,1000000)/1000000
    return(x,y)

for i in range(n):
    x,y = rand_coords()
#    print(x,y)
    if x**2+y**2 > 1:
        counter['outside']+=1
    elif x**2+y**2 <= 1:
        counter['inside']+=1

print(counter)
pi = 4*counter['inside']/(counter['outside']+counter['inside'])

t1 = time.time()
ttime = t1-t0
print()
print("Pi=",pi)
print("time:",ttime)

